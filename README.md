# Una guida a [Mastodon](https://mastodon.it)

<p style="text-align:center;"><h1>Cos'è Mastodon?.</h1></p>

Ho fatto del mio meglio per renderlo comprensibile, ma siete invitati a suggerire modifiche! Se avete un account Gitea, potete inviare direttamente le richieste di modifica; altrimenti, sentitevi liberi di contattarmi [su Mastodon](https://mastodon.uno/@mastodon) o via e-mail a `mastodon AT mastodon.uno`. 

## Indice dei contenuti

* [Cos'è Mastodon?](#cos-è-mastodon)
* [Com'è rispetto a Twitter?](#how-is-it-like-twitter)
* [Perchè è come l'e-mail?](#how-is-it-like-email)
* [Come non è come nessuno dei due; oppure, cosa sono le Timeline locali e federate?](#how-is-it-not-like-either-of-those-or-what-are-the-local-and-federated-timelines)
* [Che cos'è il Fediverse?](#what-is-the-fediverse)
* [Come faccio a stabilire la presenza del mio marchio su Mastodon?](#how-do-i-establish-my-brands-presence-on-mastodon)
* [Come faccio a essere verificato su Mastodon?](#how-do-i-get-verified-on-mastodon)
* [Ok, come faccio a far sapere alla gente che sono chi dico di essere?](#okay-how-do-i-let-people-know-that-i-am-who-i-say-i-am)
* [COME SI SCEGLIE UN'ISTANZA?](#how-do-i-pick-an-instance)
* [Come faccio a nominare qualcuno che non è nella mia istanza?](#how-do-i-mention-someone-whos-not-on-my-instance)
* [Quali sono le regole?](#what-are-the-rules)
* [Cosa succede se vedo qualcuno che infrange le regole?](#what-if-i-see-sone-breaking-the-rules)
* [Quali sono i diversi tipi di post?](#cosa-sono-i-diversi-tipi-di-post)
* [Come funzionano le impostazioni della privacy?](#how-do-privacy-settings-work)
* [Quanto è privato "Private"?](#how-private-is-private)
* [Cosa succede quando qualcuno mi segue?](#what-happens-when-someone-follows-me)
* [Se blocco il mio account, solo le persone approvate possono vedere i miei post?](#so-if-i-lock-my-account-only-approved-people-can-see-my-posts)
* [Cosa succede se qualcuno mi segue o interagisce con me e io non lo voglio?] (#what-if-someones-following-or-interacting-withme-and-i-dont-want-them-to)
* [Come faccio a sapere se qualcuno mi ha silenziato o bloccato?](#how-will-I-know-if-someones-muted-or-blocked-me)
* [E se supero il limite di caratteri?](#what-if-i-goover-the-character-limit)
* [Cosa sono gli hashtag?](#what-are-hashtag)
* [Cosa significa "CW"?](#what-does-cw-mean)
* [Ho appena allegato un'immagine al mio toot. Cosa c'è nella nuova icona "occhio"?](#i-just-attached-a-picture-to-my-toot-whats-with-the-new-eye-icon)
* [Ho appena allegato un'immagine al mio toot. Come posso aggiungervi una didascalia?](#i-just-attached-a-picture-to-my-toot-how-can-i-add-a-caption-to-it)
* [Perché dovrei aggiungere una didascalia alla mia foto?](#why-should-i-add-a-caption-to-my-picture)
* [Come mai il mio amico su un'altra istanza può usare questa emoji, ma io no?](#how-come-mio-amico-su-un'altra-istanza-può-usare-questa-emoji-ma-io-non-posso)
* [Perché non riesco a cercare una parola o una frase specifica?](#why-cant-i-search-for-a-specific-word-or-phrase)
* [Perché non posso citare il "toot" di qualcuno, come una citazione-tweet?](#why-cant-i-quote-someones-toot-like-a-quote-tweet)
* [La cultura dei Mastodonti sembra piuttosto strana?](#the-mastodon-culture-seems-pretty-weird)
* [Cosa significa ![:red_candle:](red_candle_sm.png)?](#what-does--mean)
* [Qual è il galateo dei mastodonti generalmente accettato?](#whats-the-generally-accepted-mastodon-etiquette)
* [Mi piace mastodon ma preferisco l'aspetto di twitter](#i-like-mastodon-but-i-prefer-the-way-twitter-looks)
* [Mi piace mastodon ma voglio usarlo sul mio telefono] (#i-like-mastodon-but-i-want-to-use-it-on-my-phone)
* [Come posso fare il backup del mio account Mastodon?](#how-can-i-back-up-my-mastodon-account)
* [Posso usare la tastiera per navigare in Mastodon?](#can-i-usare-la-tastiera-per-navigare-mastodon)
* [Come funzionano i preferiti su altri tipi di server?](#how-do-favorites-work-on-other-server-types)
* [Strano, il mio server non ha un mucchio di queste caratteristiche](#weird-my-server-doesnt-have-a-bunch-of-these-features)
* [Ho altre domande](#i-have-other-questions)
* [Come posso contribuire alla Guida?](#how-can-i-contribute-to-the-guide)

## Cos'è Mastodon?

Mastodon è un social network in stile Twitter combinato con server istanziati in stile e-mail. Prende il nome dalla band metal, ma ha come tema la megafauna estinta.

## Com'è rispetto a Twitter?

Si pubblicano aggiornamenti di stato relativamente brevi e si può vedere un elenco in streaming degli aggiornamenti di stato dei propri amici. È possibile conservare le notifiche (risposte, boost, preferiti e DM) in una colonna separata.

Gli stati di Mastodon sono chiamati "toot", come quelli di Twitter sono chiamati "tweet". Un toot può essere lungo fino a 500 caratteri.

Mastodon supporta anche gli hashtag, che sono parole precedute da #, come "#gameing" o "#pineapple". È possibile fare clic su un hashtag per cercare altri post contenenti quel tag.

## Perchè è come l'e-mail?

Ogni istanza di Mastodon è indipendente ma collegata in rete, come i server di posta elettronica. Se vi registrate per un account di posta elettronica su gmail.com, non avrete automaticamente un account su hotmail.com o aol.com, ma potrete inviare e ricevere messaggi da e verso utenti di hotmail.com e aol.com. 

Allo stesso modo, se vi registrate per un account su mastodon.social, non avrete un account su tutte le altre istanze, ma potrete parlare con gli utenti delle altre istanze e loro potranno parlare con voi. 

Potete creare account su più istanze se volete parlare di cose diverse separatamente. Si può avere un account su https://cybre.space per parlare di tecnologia, un account su https://elekk.xyz per parlare di giochi e un account su https://mastodon.social per le chiacchiere in generale. È necessario accedere a ciascun account separatamente e tenerlo aperto in una scheda o finestra del browser separata.

Tenete presente che in generale, quando si parla di Mastodon, "istanza" e "server" significano la stessa cosa.

## Come non è come nessuno dei due; oppure, Cosa sono le linee temporali locali e federate?

Mastodon ha due timeline aggiuntive che si possono visualizzare: la timeline locale e la timeline federata.

La timeline locale è costituita da tutti i post con stato pubblico pubblicati dagli utenti della vostra istanza, a eccezione delle risposte. (Una risposta è un messaggio pubblicato in risposta a un altro messaggio, NON un messaggio che menziona semplicemente un altro utente).

La timeline federata è costituita da tutti i post con stato pubblico pubblicati da qualsiasi utente di cui la vostra istanza è a conoscenza, anche da altre istanze. La vostra istanza conosce un utente remoto se almeno un utente della vostra istanza lo ha mai seguito.

Le timeline locali e federate possono talvolta trasformarsi in cascate di fuoco. Fate attenzione!

## Cos'è il Fediverso?

~~Purtroppo non si può dire a nessuno che cos'è il Fediverso.

Il Fediverso è la vasta gamma di server che comunicano attraverso i protocolli ActivityPub. (In generale, a meno che non stiate creando o modificando un software per interagire con il Fediverso, non avete bisogno di sapere cosa siano). Ci sono molti tipi diversi di software server sul fediverse, come Pixelfed, Pleroma, Misskey o WriteFreely. Mastodon è uno dei più popolari ed è l'argomento di questa guida. Se utilizzate uno degli altri, questa guida probabilmente non vi sarà di grande aiuto.

## Come faccio a stabilire la presenza del mio marchio su Mastodon?

La risposta breve: **Con molta attenzione.

È da un decennio che Twitter, Facebook e chissà quante altre piattaforme di social media sono diventate piattaforme per l'ottimizzazione dei motori di ricerca, la consapevolezza del marchio e la sinergia aziendale, e sarò sincero: siamo davvero, davvero stanchi di tutto questo.

Mastodon non si occupa di trasformare i follower in clienti. Non si tratta di SEO o di fedeltà al marchio. **Non trattate le persone come potenziali clienti che potrebbero acquistare i vostri prodotti; trattatele come *persone* che potreste voler conoscere.

Se siete una persona che produce o fa qualcosa e pensate che qualcuno potrebbe voler acquistare il vostro prodotto o utilizzare il vostro servizio, bene! Potete parlarne alle persone, ma *trattatele come persone*, non come obiettivi di marketing.

C'è un comportamento che le aziende adottano spesso e che fa impazzire le persone, e che non dovreste assolutamente adottare: **non inviare messaggi non richiesti a persone che pubblicizzano il vostro prodotto o servizio**. Conoscete qualcuno, intrattenete conversazioni con lui e, se pensate che qualcuno che conoscete possa essere interessato e non sia già a conoscenza di ciò che fate, *allora* parlategli di ciò che fate o di ciò che producete.

Se rappresentate un'azienda e il vostro capo vi ha detto che la vostra azienda ha bisogno di una presenza su Mastodon, ecco cosa potete dirgli (e potete dirgli che vi ho detto di dirlo):

> Mastodon non funziona proprio così. Se vogliamo avere una presenza su Mastodon, deve essere una persona libera di *essere* una persona su quell'account, invece di un portavoce aziendale. Ciò significa che la persona che gestisce l'account di Mastodon non andrà d'accordo con tutti (anche se dovrebbe certamente provarci) e tratterà gli altri utenti come persone invece che come obiettivi di marketing. Se siete d'accordo, lo farò subito. In caso contrario, è necessario fare ulteriori ricerche su Mastodon prima di stabilire una presenza sul sito.

## Come si fa a essere verificati su Mastodon?

Non esiste un account verificato su Mastodon. Si presume che tu sia chi dici di essere. Se vedi qualcuno con un segno di spunta accanto al suo nome (come ✅), significa che ha semplicemente digitato quell'emoji nel suo nome visualizzato. 

Se qualcuno si sta spacciando per te, contatta l'amministratore dell'istanza in cui si trova per risolvere il problema.

## Ok, come faccio a far sapere agli altri che sono chi dico di essere?

Molte istanze di Mastodon consentono di aggiungere metadati al profilo: fino a quattro elementi visualizzati in una tabella nella pagina del profilo, che non contano nella lunghezza del testo del profilo. Se utilizzate questi campi per collegarvi ad altri siti web, alcune istanze vi permetteranno di verificare che siete *proprietari* di quei siti web fornendo un link al vostro account Mastodon. Per esempio, se avete un sito web personale, potete includere il link al vostro sito web nei metadati di Mastodon e poi includere un link al vostro account Mastodon nell'intestazione del vostro sito web, e Mastodon verificherà che siete la persona che possiede il vostro sito web.

Un'istanza di Mastodon che lo consente avrà delle istruzioni nella pagina **Modifica profilo** che indicano come aggiungere il link di verifica.

# COME SCELGO UN'ISTANZA?

Questa è grossa di proposito.

Scegliere un'istanza può essere difficile. Molte istanze hanno un obiettivo specifico: `mastodon.lol` è uno spazio sicuro per i gay, `hackers.town` è un rifugio per chi tocca i computer, `wandering.shop` è costruito per gli appassionati di fantascienza e fantasy e `botsin.space` si concentra sull'hosting e lo sviluppo di bot automatizzati. Su `oulipo.social` è illecito postare un toot contenente una qualsiasi "e". 

Se non avete ancora creato un account su mastodon, potreste trovare utile provare una delle istanze più grandi, come `mastodon.uno` - l'istanza di punta, con oltre 20.000 utenti - o `mastodon.social` . Queste istanze hanno popolazioni ampie e solitamente amichevoli che vi aiuteranno a trovare un'istanza più di nicchia, se è quello che state cercando. Attenzione, però, perché a causa delle loro dimensioni, le linee temporali locali di istanze di nicchia possono muoversi *molto* lentamente.

(*Una breve nota su mastodon.uno: è **la più grande istanza italiana di uso generale (la più grande in assoluto è un'istanza in lingua giapponese, [mstdn.jp](https://mstdn.jp)). Molte persone vanno lì e non controllano mai le altre istanze. Se create un account su mastodon.social, consideratelo come un punto di passaggio temporaneo mentre trovate un'istanza più adatta alle vostre esigenze e ai vostri interessi. Una volta trovata, potete esportare tutte le persone che seguite, silenziate e bloccate su mastodon.social e importarle nel vostro nuovo account, in modo da non dover andare in giro a cercare di nuovo tutti).

Se vi siete già registrati su un'istanza ma non siete sicuri che sia adatta a voi, provate a chiedere in giro se ci sono istanze che potrebbero essere più adatte a voi. Inoltre, provate a cercare un #hashtag che vi interessa; se vedete molte persone su un'istanza che parlano di quell'argomento, potrebbe essere un buon posto per voi.

Potete anche provare l'elenco di istanze italiane su [mastodon.it](https://mastodon.it/it/istanze-mastodon-italiane) o l'elenco per temi su [Gitea](https://gitea.it/Mastodon/Istanze-Mastodon), anche se ci sono *molte* istanze elencate e potreste essere sopraffatti. Procedete con calma.

## Come faccio a nominare qualcuno che non è nella mia istanza?

Mastodon usernames take the form @*username*@*instanza*. My account on mastodon.social is @<span>noelle</span>@mastodon.social; my account on elekk.xyz is @<span>noelle</span>@elekk.xyz. If you're mentioning someone on a different instance, you have to type the whole thing (although the toot input box will help you auto-complete the username if it's a name the instance knows already). 

If you're mentioning someone on your own instance, you just have to type the first part; if you're on elekk.xyz, @noelle will get to me just like @<span>noelle</span>@elekk.xyz will. If you leave off the "@*instance*" Mastodon understands that you want to talk to the local user.

## What are the rules?

The rules depend on which instance you're on. Each instance has a page at https://<span>*instance*</span>/about/more that usually contains more information about the instance and often describes the community guidelines. For example, mastodon.social has its community guidelines posted at https://mastodon.social/about/more .

Keep in mind that these are usually guidelines and not hard-and-fast rules. Since each instance is run by a separate team of moderators - often just one person! - they have the final say over what's allowed and not allowed on their instance. Gli amministratori della vostra istanza potrebbero persino arrivare a bloccare un'intera altra istanza se i suoi utenti si rivelassero incompatibili con i valori della vostra istanza e i moderatori dell'altra istanza non volessero aiutarvi.

Si pubblicano aggiornamenti di stato relativamente brevi e si può vedere un elenco in streaming degli aggiornamenti di stato dei propri amici. È possibile conservare le notifiche (risposte, boost, preferiti e DM) in una colonna separata.

Gli stati di Mastodon sono chiamati "toot", come quelli di Twitter sono chiamati "tweet". Un toot può essere lungo fino a 500 caratteri.

Mastodon supporta anche gli hashtag, che sono parole precedute da #, come "#gameing" o "#pineapple". È possibile fare clic su un hashtag per cercare altri post contenenti quel tag.

## Cosa succede se vedo qualcuno che infrange le regole?

Se si vede qualcuno che infrange le regole, è possibile inviare una segnalazione. Fare clic sul pulsante `...` sotto il messaggio incriminato e selezionare `Report @user` (che di solito si trova in fondo al menu). Si aprirà una finestra in cui è possibile selezionare altri messaggi recenti di quell'utente, se necessario, e anche digitare una *ragione* per la segnalazione, in modo che il moderatore che riceve la segnalazione capisca perché l'avete inviata. 

Se l'utente proviene da un'altra istanza, sotto il motivo della segnalazione si vedrà anche un interruttore accanto a "Inoltra a *loro istanza*". In questo modo non solo si segnala l'utente ai moderatori della propria istanza, ma si invia la segnalazione anche all'istanza dell'altro utente. Prima di farlo, **controllare le regole dell'istanza in cui si trova**. È possibile che non stiano violando le regole locali.

**I moderatori e gli amministratori della *tua* istanza potranno vedere che sei stato tu a inviare la segnalazione. Se si inoltra la segnalazione all'istanza di un utente remoto, i moderatori e gli amministratori di quell'istanza *non* potranno vedere chi ha inviato la segnalazione, ma solo che proviene dalla propria istanza.**

Per impostazione predefinita, l'utente non riceverà alcuna notifica sulle azioni che i moderatori o gli amministratori intraprendono in risposta alle sue segnalazioni. Potete chiederlo, ma preparatevi a sentire qualcosa del tipo "non commentiamo come risolviamo le segnalazioni".

## Quali sono i diversi tipi di messaggi?

I post di Mastodon (e di altri fediverse) possono essere concatenati in diversi modi. Uso tre termini per riferirmi a diversi tipi di messaggi. Questi termini potrebbero non essere quelli usati da altre persone.

* **Posti autonomi** sono messaggi che non sono risposte a nessun altro messaggio. (Sono a capo di una catena. È possibile includere i nomi utente delle persone nei post autonomi per etichettarle; ciò non rende il post una risposta. **Per esempio: ** si usa la casella di composizione standard ("Cosa ti passa per la testa?" nelle impostazioni predefinite di Mastodon) per creare un nuovo post.
* Le **auto-risposte** sono post che sono una risposta diretta a *uno dei propri* post autonomi o a un'altra auto-risposta. Anche in questo caso, possono includere riferimenti a un'altra persona. Si può continuare a rispondere ai propri messaggi finché si vuole, e *fino a quando il messaggio di qualcun altro non si trova al di sopra di quello che si sta scrivendo nella catena*, continuerà a essere un'auto-risposta. **Per esempio: ** si clicca sul pulsante di risposta di uno dei propri messaggi autonomi per rispondere a se stessi, poi si clicca sul pulsante di risposta di *questo* messaggio per rispondere di nuovo a se stessi.
* Le **risposte** sono messaggi che sono risposte a qualcun altro *o risposte a una risposta a qualcun altro*. **Per esempio: **si clicca sul pulsante di risposta sul post di qualcun altro per rispondergli, **o** si clicca sul pulsante di risposta su uno dei propri post che è una risposta al post di qualcun altro.

Ho incluso un diagramma [qui] (replies.png) (è un link perché è grande). Notate come non appena il post di qualcun altro entra nella catena, le vostre risposte smettono di essere auto-risposte. Questo è importante, perché le auto-risposte e le risposte funzionano in modo diverso nelle timeline dei vostri follower.

## Come funzionano le impostazioni sulla privacy?

Sotto ogni post sono presenti tre icone: una fotocamera, un globo o un lucchetto e le lettere "CW". Fare clic sul globo o sul lucchetto per scegliere le impostazioni della privacy per il proprio post. *È possibile impostare il livello di privacy predefinito per i propri messaggi in **Preferenze > Altro > Privacy dei messaggi**.

**Pubblico** significa che tutti possono vedere il vostro messaggio. Apparirà nel vostro elenco di post e nelle timeline dei vostri follower. I vostri *post* e le vostre *risposte* che sono pubbliche appariranno nel vostro elenco di post, nelle timeline Home dei vostri follower, nelle timeline pubbliche locali e federate e nelle Menzioni di chiunque menzioniate con il vostro nome utente. Le vostre *risposte* che sono pubbliche appariranno nel vostro elenco di *toots e risposte* (*non* l'elenco primario di toots; è un elenco separato!), nelle timeline Home di tutti i vostri follower che seguono *anche* la persona a cui state rispondendo e nelle Menzioni di chiunque menzionate con il nome utente.
* Non in elenco** significa che tutti possono vedere il vostro post, ma non apparirà nelle timeline pubbliche, né in quelle locali né in quelle federate. A parte questo, i post non elencati si comportano esattamente come i post pubblici.
* Solo per i follower** significa che solo le persone che vi seguono e le persone menzionate nel post possono vedere il vostro post nelle loro timeline o nella pagina del vostro profilo. Se qualcuno che non vi segue visualizza il vostro profilo, non vedrà i post dei soli follower. *Per i vostri follower*, i post di soli follower si comportano esattamente come i post non in elenco. **I tuoi post di soli follower che menzionano un altro utente appariranno anche nelle menzioni di quell'utente, anche se non ti segue!
**Privato** significa che solo le persone menzionate nel vostro post possono vederlo. Apparirà nelle loro menzioni e, sui server Mastodon superiori alla versione 3.0, apparirà nella colonna dei Messaggi diretti.

Tenete presente che alcuni server, che utilizzano un software compatibile ma non uguale a Mastodon, ignoreranno queste impostazioni di privacy se inviate un messaggio ai loro utenti, quindi fate attenzione!

## Quanto privato è "Privato"?

Non lo sottolineerò mai abbastanza: **I messaggi privati non sono criptati o sicuri.

L'amministratore del vostro server può essere in grado di leggere *qualsiasi* messaggio inviato sul loro server, così come *qualsiasi* messaggio inviato a un utente sul loro server. **Tuttavia**, è una rottura di scatole farlo. I vostri messaggi privati non appariranno nel pannello di amministrazione del sito web; per accedervi, l'amministratore deve accedere manualmente e direttamente al database, in genere collegandosi alla riga di comando del server. Questo non è qualcosa che gli amministratori fanno per capriccio; lo fanno solo quando devono assolutamente farlo, e questo è il motivo:

È una precauzione di sicurezza necessaria. Gli amministratori *non vogliono* leggere i vostri toot privati, ma devono essere *possibili* perché altrimenti i toot privati permettono ad alcuni utenti di molestare segretamente altri utenti o di condurre affari illegali all'insaputa dell'amministratore, e secondo molte leggi l'amministratore sarà responsabile di aver permesso la molestia o il comportamento illegale *anche se non sapeva che stava accadendo*.

Detto questo, in generale, l'amministratore controllerà i toot che avete contrassegnato come privati solo se ha motivo di credere che siano in corso molestie o affari illeciti. Assicuratevi di avere fiducia che il vostro amministratore agisca in questo modo, e se non ne avete, potrebbe essere il momento di cercare un'altra istanza.

Già che ci siamo, vale la pena notare che questo vale per quasi tutti i software di social media. Gli amministratori di Twitter possono leggere i vostri DM. Gli amministratori dei forum possono leggere i messaggi privati. Questa non è una caratteristica nuova o insolita di Mastodon; mi sto solo assicurando che lo sappiate.

**Come regola generale, se un'applicazione che state usando non è [peer-to-peer](https://en.wikipedia.org/wiki/Peer-to-peer) e si affida a un intermediario come un server, le informazioni che state inviando non sono sicure, a meno che non prendiate misure aggiuntive al di fuori dell'applicazione per proteggerle.

## Cosa succede quando qualcuno mi segue?

~~Se qualcuno vi segue, avete un debito di vita nei confronti di quella persona. Dovrete dare la vostra vita per quella persona quando ne avrà bisogno. Una volta fatto ciò, la persona in questione non vi seguirà più e, se siete sopravvissuti, sarete liberi di continuare la vostra vita normalmente.

Scherzo.

Se qualcuno vi segue, vedrà i vostri post sulla sua timeline Home e potrà vedere i post dei soli follower. Se volete, potete limitare le persone che possono seguirvi cliccando su **Modifica profilo** e selezionando **Blocca account**, che vi permetterà di approvare e rifiutare manualmente le persone che vogliono seguirvi.

## Quindi se blocco il mio account, solo le persone approvate possono vedere i miei post?

Sì e no.

Con un account bloccato, potete approvare chi può seguirvi *attraverso l'interfaccia di Mastodon*. Ciò significa che solo le persone approvate potranno vedere i post dei **soli follower**. I tuoi post **non elencati** appariranno comunque sul tuo profilo e i tuoi post **pubblici** appariranno comunque sul tuo profilo e sulle timeline locali e federate.

**Tuttavia.

Ogni account di Mastodon (su un server non modificato) crea anche un feed RSS dei suoi post pubblici e non elencati - cioè i post che appaiono sul profilo dell'account. Non include i toot dei soli follower o i messaggi diretti, e se avete [messo una CW su un toot](#cosa-significa-cw), solo la CW appare nel feed RSS, non quello che c'è sotto. 

Il tuo feed RSS appare a `https://<il tuo server>/users/<il tuo nome utente>.rss`; per esempio, dato che io sono `https://elekk.xyz/@noelle`, il mio feed RSS è `https://elekk.xyz/users/noelle.rss` (ricordati di togliere la `@`!).

**Chiunque può abbonarsi a questi feed usando un lettore RSS per vedere i vostri post pubblici e non elencati quando li pubblicate.** Non potete controllare chi può vedere questi feed, ma non contengono (e, per design, non possono contenere) i vostri post privati o riservati ai soli follower. *Se si pubblicano solo i toot dei follower, il feed RSS sarà vuoto.

Ricordate che potete impostare il livello di privacy predefinito per i vostri post in **Preferenze > Altro > Privacy dei post**. Se avete un account bloccato, potreste preferire l'impostazione predefinita di soli follower, in modo da dover fare uno sforzo attivo per pubblicare un toot non elencato o pubblico.

## Cosa succede se qualcuno mi segue o interagisce con me e io non voglio che lo faccia?

Avete un paio di opzioni.

* Se non volete più vederli nel vostro feed**, potete "silenziarli". In questo modo si eviterà che i suoi post appaiano in tutti i feed; è possibile bloccare facoltativamente le notifiche (Preferiti, Incrementi e Menzioni) da parte loro, in modo che se non si vogliono vedere i post di qualcuno ma si vogliono vedere i suoi tentativi di interagire con voi, è possibile.
* Se non volete vederli e non volete che vi vedano**, potete *bloccarli*. Questo li disattiverà automaticamente; se vi stavano seguendo e/o voi li stavate seguendo, interromperà anche questi. Non potranno seguirvi o comparire in *qualsiasi* feed a meno che non li sblocchiate. (Ma vedi sotto).
* Se vi stanno molestando o stanno infrangendo le regole**, potete [segnalarli] (#cosa-se-vedo-qualcuno-che-infrange-le-regole) e, auspicabilmente, il vostro moderatore se ne occuperà.

Tutte e tre queste opzioni sono disponibili cliccando sul pulsante `...` sotto uno dei toot dell'utente o sul suo profilo all'interno dell'interfaccia web di Mastodon.

**Tuttavia.

Proprio come con un [account bloccato] (#so-if-i-lock-my-account-only-approved-people-can-see-my-posts), qualsiasi utente - anche quelli che avete bloccato o che sono stati sospesi da un moderatore - può andare alla vostra pagina pubblica o al vostro feed RSS per vedere i vostri toots pubblici e non elencati. Purtroppo non c'è un buon modo per ovviare a questo problema, se non quello di rendere tutti i vostri toot solo per i follower.

## Come faccio a sapere se qualcuno mi ha silenziato o bloccato?

Non riceverete una notifica se qualcuno vi ha silenziato o bloccato.

Se qualcuno ti ha silenziato, non c'è modo di saperlo. Se non vi rispondono costantemente quando menzionate il loro nome utente, potreste insospettirvi, ma Mastodon rende deliberatamente quasi impossibile sapere se siete stati silenziati. (Tra l'altro, questo è un espediente per cercare di evitare che qualcuno vi molesti creando più account per aggirare i silenzi).

Se qualcuno vi ha bloccato, non lo seguirete più, i suoi post non appariranno nel vostro feed e quando visualizzerete il profilo del suo account *nell'interfaccia web di Mastodon*, nessuno dei suoi post verrà caricato. (Vale la pena notare che se state guardando il profilo di una persona che *non* seguite, a volte i suoi post non vengono caricati ed è solo perché il server è lento, non perché vi ha bloccato, quindi non siate *troppo* veloci nel fare una supposizione).

Se qualcuno vi ha bloccato, potete comunque andare sulla sua pagina pubblica e vedere i suoi toot pubblici e non elencati; i profili pubblici non richiedono l'autenticazione (cioè non dovete essere registrati in quell'istanza per vederli) e quindi non possono dire chi siete o che l'utente vi ha bloccato. **Detto questo, siamo onesti. Se qualcuno vi ha bloccato, non vi vuole in giro. Puoi continuare a leggere i suoi post pubblici e non elencati, ma forse non lo fai? In sostanza, stai invadendo la loro privacy e stai deliberatamente oltrepassando un limite che hanno stabilito, e "il software me lo permette, quindi deve andare bene" è una giustificazione piuttosto inconsistente. Lasciateli in pace, per favore.

## E se supero il limite di caratteri?

Non preoccuparti. Innanzitutto, non puoi; Mastodon non ti permetterà di inviare un messaggio che superi il limite di caratteri dell'istanza. Non vi metterete nei guai o altro.

Se vi accorgete che quello che volete dire è troppo lungo per un singolo toot, o se vi viene in mente qualcos'altro dopo aver postato un toot, potete *rispondere al vostro stesso toot*. Mastodon supporta i thread di toot, per cui potete fare tutti i toot che volete, rispondendo a ogni toot in sequenza, e l'intera serie verrà visualizzata quando qualcuno cliccherà su uno qualsiasi dei toot nella discussione.

Quindi, se il vostro toot è troppo lungo, dividetelo e fate in modo che la seconda metà sia una risposta al primo; se poi vi viene in mente qualcos'altro, rispondete al toot originale e la risposta apparirà ogni volta che qualcuno cliccherà sul toot originale.

## Cosa sono gli hashtag?

Per creare un hashtag, digitate "#" e poi un numero qualsiasi di lettere o numeri. Gli accenti contano, la punteggiatura, gli spazi, i simboli e le emoji no. #howismydaygoing è un hashtag valido; #höwísmydàygôíng è valido; #how-is-my-day-going non lo è (si cattura solo #how).

Un hashtag è un metadato sul vostro toot: fornisce informazioni aggiuntive che non appartengono necessariamente al corpo del toot, ma che sono utili per la sua comprensione. Se siete programmatori, è una sorta di commento al codice.

Come bonus†, gli hashtag sono tracciati da ogni istanza. Facendo clic su un hashtag si accede a un elenco di post pubblici con quell'hashtag. Potete usarli per seguire la #politica, controllare gli utenti raccomandati da #FollowFriday o vedere le opere d'arte delle persone usando #mastoart.

Non esagerate con gli hashtag. Come linea guida, gli hashtag non dovrebbero superare il 10% della lunghezza totale del vostro messaggio. Se vi accorgete di aver superato questa soglia, forse state esagerando un po' troppo con gli hashtag.

*Questo era in realtà l'intento originario degli hashtag, ma da allora l'uso è cambiato.

## Cosa significa "CW"?

CW è l'acronimo di Content Warning. Nasconde il vostro post dietro un testo (che potete scegliere voi); è come un link Read More.

Si possono usare le CW per:

* Politica
* Sesso
* Argomenti volgari
* Fobie comuni, come i ragni o il sangue
* Discussioni sulla salute
* Battute di spirito per barzellette
* Messaggi lunghi che potrebbero altrimenti riempire le timeline delle persone
* Commento a discussioni in corso altrove nel fediverse, spesso con la CW "meta" o "discourse".

Alcune abbreviazioni comuni che troverete nelle CW sono:

* mh: salute mentale
* ph: salute fisica
* alc: alcool
* pol: politica, a volte con l'aggiunta del luogo, come "uspol" che significa politica degli Stati Uniti.
* pda: dimostrazione pubblica di affetto
* nsfw: not safe for work
* ec: contatto visivo, di solito usato quando c'è una fotografia allegata.

In generale, usate il vostro giudizio migliore; pensate "c'è un motivo per cui qualcuno potrebbe non volerlo vedere?". Avete l'opportunità di prendere un momento in più e di rendere il fediverse un posto più bello per le persone. Perché non dovreste cogliere questa opportunità?

**Una nota importante:** Mastodon *non* tiene traccia degli hashtag presenti nel testo di una CW. Mastodon tiene traccia degli hashtag che si trovano *sotto* una CW. Inserite sempre gli hashtag nel corpo del vostro messaggio, mai nel contenuto.

## Ho appena allegato un'immagine al mio toot. Cos'è questa nuova icona "occhio"?

Facendo clic su di essa, l'immagine viene nascosta dietro una scritta "Contenuto sensibile". Questo è utile per la nudità, il gore e la violenza, gli argomenti politici, ecc.

Noterete che se in un toot sono presenti sia un'immagine che una CW, l'overlay "Contenuti sensibili" viene attivato automaticamente e non può essere disattivato. Questo è fatto apposta.

## Ho appena allegato un'immagine al mio toot. Come posso aggiungere una didascalia?

Quando si allega un'immagine, si vedrà "Modifica" (più l'icona di una matita) in alto a destra dell'immagine. Facendo clic su di essa si apre una finestra di dialogo che consente di determinare quale parte dell'immagine deve essere visualizzata nell'anteprima; consente inoltre di impostare un testo alt per l'immagine, che le persone possono leggere se passano con il mouse sul testo e che i lettori di schermo (ad esempio per gli ipovedenti) possono leggere invece di dire semplicemente "immagine incorporata".

Il testo nella casella di descrizione ha un proprio limite di caratteri; non **conta** il limite di caratteri per il toot!

## Perché dovrei aggiungere una didascalia alla mia immagine?

In una parola: per l'accessibilità. 

Alcune persone che usano Mastodon sono ipovedenti e usano screen reader. Alcune persone che usano Mastodon hanno le immagini disattivate per risparmiare l'uso dei dati. A volte si verificano errori del disco o del server, oppure l'amministratore decide di eliminare i vecchi file e l'immagine non viene più caricata. La didascalia di un'immagine permette a chi si trova in queste condizioni di partecipare ai vostri toots con un contesto completo.

È anche possibile utilizzare le didascalie delle immagini per inserire battute aggiuntive (come fanno spesso i webcomics) o commenti supplementari sull'immagine. Approfittate del fatto che le descrizioni delle immagini hanno un limite di caratteri a parte e inseriteci quello che volete. Il limite è il cielo.

## Come mai il mio amico su un'altra istanza può usare questa emoji, ma io no?

Ogni istanza può definire emoji personalizzate per i propri utenti e molti ne hanno approfittato. L'amministratore dell'istanza può copiare le emoji che preferisce da altre istanze. Se vedete un emoji che vi piace e non è disponibile sulla vostra istanza, chiedete all'amministratore di copiarlo.

## Perché non posso cercare una parola o una frase specifica?

È una funzione anti-molestie. I molestatori spesso cercano parole o frasi particolari (come "TERF" o "omofobo" o "supremazia bianca") per attaccare e infierire sulle persone con cui non sono d'accordo. Limitando la ricerca ai nomi utente e agli hashtag, Mastodon permette agli utenti di decidere come far apparire i loro messaggi nelle ricerche degli altri. (Sebbene alcune istanze di Mastodon permettano la ricerca full-text, è possibile cercare solo i propri toot su quelle istanze. Questo rende più facile trovare qualcosa che avete postato tempo fa senza esporvi a molestie).

## Perché non posso citare i toot di qualcuno, come un tweet?

Come la ricerca, è una funzione anti-molestie. Se volete rispondere a un toot di qualcuno, dovete rispondere davvero; non potete semplicemente trasmetterlo ai vostri follower con un commento sprezzante.

*Non cercate di aggirare l'ostacolo facendo lo screencapping dei messaggi e allegandoli come immagini. Potete farlo, ma la comunità di Mastodon tende a non vederlo di buon occhio e vi farete una cattiva reputazione molto rapidamente se continuerete a farlo.)

## La cultura di Mastodon sembra piuttosto strana?

Può esserlo! Ma può diventare una stranezza confortevole.

Ecco alcune stranezze comuni:

* :pineapple:: nessuno lo sa veramente. [acw](https://cybre.space/@acw) l'ha postata, qualcun altro l'ha ripresa e da allora è andata avanti. È solo una sciocchezza. In caso di dubbio, basta postare un :pineapple:.
* AWOO: [Awoo.Space](https://awoo.space) era una delle prime istanze dei Mastodon. "Awoo" è il suono di un lupo che ulula. È divertente dirlo. Awoo! (Pawoo.net non ha nulla a che fare con questo; in giapponese, "pawoo" è il suono della tromba di un elefante). Qualcuno si è irritato per gli awoo e ha istituito una multa di 350 dollari per chi fa awoo. Nessuno l'ha mai pagata, non preoccupatevi.
#gioco: C'era (e c'è ancora) la percezione che il #gaming sia "hardcore" ed elitario, e c'era (e c'è) il timore che certe razze di videogiocatori sovrastino l'hashtag #gaming. #gameing è stato un attacco preventivo; si tratta di un approccio divertente, spensierato e accessibile ai giochi, video e non.
* IT'S BEEN/SOME come CW: un riferimento rispettivamente alla canzone dei Barenaked Ladies [*One Week*] (https://www.youtube.com/watch?v=fC_q9KPczAg) e alla canzone degli Smash Mouth [*All Star*] (https://www.youtube.com/watch?v=L_jWHffIx5E). Un meme comune su Mastodon è quello di inserire la prima o le prime due parole della canzone nella CW e poi sovvertire le aspettative postando qualcos'altro sotto il taglio.

Ci si abitua.

## Cosa significa ![:red_candle:](red_candle.png)?

All'inizio di novembre 2017 abbiamo perso un popolare mastodontico per suicidio. ![:red_candle:](red_candle_sm.png) e ![lattentacle](lattentacle_sm.png) commemorano la nostra amica Natalie Nguyen.

## Qual è il galateo dei Mastodonti generalmente accettato? 

Non ci sono regole ferree per tutti e (come già detto) le diverse istanze hanno linee guida diverse. Detto questo, molte persone seguono alcune semplici linee guida volte a rendere Mastodon un luogo più amichevole per tutti.

* Se si allega un'immagine al proprio toot ma non la si descrive nel toot, utilizzare [alt text](#i-just-attached-a-picture-to-my-toot-how-can-i-make-sure-its-accessible) per descrivere l'immagine in modo che le persone che utilizzano screen reader possano comprenderla.
* Se si allega un'immagine che contiene **nudità, contenuti porno o a sfondo sessuale, gore, violenza o politica**, o uno qualsiasi dei comuni fattori scatenanti della PTSD/ansia (come cibo, ragni, ecc.), [contrassegnarla come sensibile] (#i-just-attached-a-picture-toy-toot-whats-with-the-new-eye-icon).
* Se il *testo* del vostro toot contiene uno di questi argomenti, [usate un avviso di contenuto](#cosa-significa-cw).
* Non è necessario utilizzare un accorciatore di URL. Mastodon presume che tutti gli URL siano lunghi esattamente 20 caratteri. Lasciate che le persone vedano ciò che state effettivamente linkando.
* Se create un bot che pubblica automaticamente, fate in modo che pubblichi utilizzando le [impostazioni sulla privacy non elencate] (#come-possono-funzionare-le-impostazioni-sulla-privacy). In questo modo si evita che il bot venga segnalato come spam.
* Se utilizzate uno script che invia i vostri tweet da Twitter a Mastodon, impostate lo script in modo che utilizzi una CW. In questo modo si evita che argomenti sensibili (vedi sopra) vengano postati su Mastodon.
* Se vedete una conversazione e volete aggiungere un commento:
    * Cliccate sull'intera conversazione e assicuratevi che il vostro pensiero non sia già stato espresso da qualcun altro. 
    * Assicurati che il tuo commento sia in linea con il tono della conversazione, che sia gentile con gli altri partecipanti alla conversazione e che dia agli altri partecipanti il beneficio del dubbio.
* "noadvice", come hashtag o CW, indica che l'utente si sta solo sfogando e non cerca aiuto o suggerimenti. La simpatia e il conforto sono comunque ben accetti.
* Ricordate che se qualcuno non vi risponde, non significa che vi stia ignorando. Potrebbe essere lontano da Mastodon; potrebbe avere così tante notifiche da non vedere il vostro messaggio; potrebbe aver messo in mute la conversazione, così la vostra risposta non è nemmeno apparsa! Ci sono molte ragioni per cui qualcuno si perde un messaggio, anche se diretto a lui; non prendetela sul personale.

## Mi piacciono i Mastodon ma preferisco l'aspetto di Twitter.

Potreste trarre vantaggio da [Pinafore](https://pinafore.social/), una nuova interfaccia per Mastodon di [Nolan Lawson](https://toot.cafe/@nolan).

[Halcyon](https://notabug.org/halcyon-suite/halcyon) è un client web per Mastodon che replica l'interfaccia di Twitter. Poiché Halcyon è un software open-source, ci sono [più server che lo eseguono](https://notabug.org/halcyon-suite/halcyon#instances); potete scegliere quello che preferite. Quando si usa Halcyon si usa il proprio login Mastodon; per esempio, se si ha un account su elekk.xyz, si usa `your-account@elekk.xyz` e la propria password Elekk per accedere.

Ricordate che Pinafore e Halcyon sono client di terze parti e assicuratevi di fidarvi di loro prima di fornire loro i vostri dati di accesso!

## Mi piace Mastodon ma voglio usarlo sul mio telefono.

Mastodon ha un design reattivo, quindi è possibile utilizzarlo nel browser del telefono. In alternativa, sono disponibili molte applicazioni per Mastodon, tra cui un'applicazione ufficiale "Mastodon per iPhone" rilasciata nell'agosto 2021. Le più utilizzate su iOS sono Amaroq e Toot! Su Android, provate Tusky.

*(NB: mi è stato detto che "Tootdon inoltra silenziosamente ai propri server le copie dei post con cui interagisci e il token di autenticazione del tuo account". Non so se questo sia ancora valido. Come sempre, siate prudenti quando fornite alle app le vostre informazioni.)*

## Come posso fare il backup del mio account Mastodon?

[Alex Schroeder](https://octodon.social/@kensanata) ha un eccellente [Mastodon Archiver](https://github.com/kensanata/mastodon-backup/) che vi aiuterà a fare il backup del vostro account e molto altro.

## Posso usare la tastiera per navigare in Mastodon?

Nell'interfaccia web, sì. I tasti di scelta rapida di Mastodon sono documentati in `https://<tuo-server>/web/keyboard-shortcuts`, a cui si può accedere quando si è connessi all'interfaccia web. (Il link si trova in fondo alla colonna **Iniziare a lavorare**, con l'etichetta "Tasti di scelta rapida").

## Come funzionano i preferiti su altri tipi di server?

In generale:

* Se si fa clic su "preferito" su un post di un altro tipo di server (Misskey, Pleroma, GNU Social, ecc.), il post verrà federato correttamente. Alcuni di questi server consentono più emoji di risposta e ognuno di essi seleziona un emoji "preferito" generico, che apparirà sul post del destinatario.
* Se qualcuno su un altro tipo di server seleziona un'emoji per rispondere al vostro post, questa verrà federata a voi su Mastodon come preferita, *non importa quale sia l'emoji*.

## Strano, il mio server non ha molte di queste caratteristiche.

Forse non sei su Mastodon! Il Fediverse - la vasta collezione di server collegati dal protocollo ActivityPub/OStatus - ha molti tipi diversi di server. Il vostro server potrebbe eseguire Pleroma, Misskey, GNU Social o qualcos'altro! Purtroppo non ne so molto, quindi dovrete chiedere ai loro utenti una guida introduttiva come questa.

## Ho altre domande.

Chiedete in giro! Di solito le persone sono molto felici di rispondere alle domande e di aiutare. Se sei davvero bloccato, chiedi a me: https://mastodon.uno/@mastodon

## Come posso contribuire alla Guida?

Dai un'occhiata alla [guida ai contributi](contributing.md)!
